from flask import Flask, request, jsonify, Response

from sudachipy import tokenizer
from sudachipy import dictionary

app = Flask(__name__)

tokenizer_obj = dictionary.Dictionary().create()
mode = tokenizer.Tokenizer.SplitMode.C


@app.route("/", methods=["POST"])
def sudachi_api():
    if request.method != "POST":
        return jsonify({"status": "no"})

    raw_text = request.form["text"]
    processed_result = [
        {
            "surface": m.surface(),
            "reading": m.reading_form(),
            "normalized": m.normalized_form(),
        }
        for m in tokenizer_obj.tokenize(raw_text, mode)
    ]
    res = jsonify({"status": "ok", "payload": processed_result})
    res.headers["Access-Control-Allow-Origin"] = "*"
    return res
