# 実装メモ

- `fetch()`でいろいろつまづいた

  - `body`を`key=value`の形にしていなかった
  - `body`の URL エンコードを忘れていた

    - もう一度 HTTP リクエストについて復習する

  - `fetch()`の API はそのうち慣れればいいと思う

  - https://developer.mozilla.org/en-US/docs/Web/API/Fetch_API/Using_Fetch
