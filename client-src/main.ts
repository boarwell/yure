const CLASS_SUBMIT_BUTTON = 'js-submit-button';
const ID_TEXT_AREA = 'text';
const ID_OUTPUT_AREA = 'output';
const API_URL = 'http://localhost:5000/';

type PROCESSED_TEXT = {
  surface: string;
  reading: string;
  normalized: string;
};

type API_RESPONSE =
  | {
      status: 'ok';
      payload: PROCESSED_TEXT[];
    }
  | {
      status: 'no';
    };

const isHiragana = (c: string): boolean => {
  const firstHiragana = 'ぁ'.codePointAt(0)!;
  const lastHiragana = 'ゖ'.codePointAt(0)!;
  const codePoint = c.codePointAt(0)!;

  return firstHiragana <= codePoint && codePoint <= lastHiragana;
};

const isKatakana = (c: string): boolean => {
  const firstKatakana = 'ァ'.codePointAt(0)!;
  const lastKatakana = 'ヺ'.codePointAt(0)!;
  const codePoint = c.codePointAt(0)!;

  return firstKatakana <= codePoint && codePoint <= lastKatakana;
};

// TODO: test
const kata2hira = (c: string): string => {
  const BETWEEN_KATAKANA_HIRAGANA = 96;
  const codePoint = c.codePointAt(0)!;

  return String.fromCodePoint(codePoint - BETWEEN_KATAKANA_HIRAGANA);
};

const createSpan = (surface: string): HTMLSpanElement => {
  const span = document.createElement('span');
  span.textContent = surface;
  return span;
};

const createRuby = (surface: string, reading: string): HTMLElement => {
  const ruby = document.createElement('ruby');

  const rb = document.createElement('rb');
  rb.textContent = surface;
  ruby.appendChild(rb);

  const rt = document.createElement('rt');
  rt.textContent = [...reading]
    .map(c => (isKatakana(c) ? kata2hira(c) : c))
    .join('');
  ruby.appendChild(rt);

  return ruby;
};

const furigaBuilder = ({
  surface,
  reading
}: Omit<PROCESSED_TEXT, 'normalized'>) => {
  if (reading === '' || [...surface].every(isHiragana)) {
    return createSpan(surface);
  }

  return createRuby(surface, reading);
};

const renderResult = (res: API_RESPONSE): void => {
  if (res.status === 'no') {
    console.warn('failed text processing on server...');
    return;
  }

  const outputArea = document.querySelector(`#${ID_OUTPUT_AREA}`);
  if (outputArea === null) {
    throw new Error(`${ID_OUTPUT_AREA} is missing...`);
  }

  const fragment = document.createDocumentFragment();
  for (const { surface, reading } of res.payload) {
    fragment.appendChild(furigaBuilder({ surface, reading }));
  }

  if (outputArea.hasChildNodes()) {
    outputArea.innerHTML = '';
  }
  outputArea.appendChild(fragment);
};

const post2API = async (value: string): Promise<API_RESPONSE> => {
  const res = await fetch(API_URL, {
    method: 'POST',
    mode: 'cors',
    headers: {
      'Content-Type': 'application/x-www-form-urlencoded'
    },
    body: encodeURI(`text=${value}`)
  });

  return res.json() as Promise<API_RESPONSE>;
};

const submitHandler = async (): Promise<void> => {
  const textArea = document.querySelector(`#${ID_TEXT_AREA}`);
  if (textArea === null) {
    throw new Error(`textarea is missing...`);
  }

  const { value } = textArea as HTMLTextAreaElement;
  if (value === '') {
    console.log(`textarea has no text...`);
    return;
  }

  renderResult(await post2API(value));
};

const main = () => {
  const submitButton = document.querySelector(`.${CLASS_SUBMIT_BUTTON}`);
  if (submitButton === null) {
    throw new Error(`${submitButton} is missing...`);
  }

  submitButton.addEventListener('click', submitHandler);
};

main();
